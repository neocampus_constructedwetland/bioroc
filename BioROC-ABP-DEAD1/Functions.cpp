// Author: Jean-Louis Druilhe (jean-louis.druilhe@univ-tlse3.fr)
// NOTE:
//
// Humidity with capacitive probe (I2C)
// Temperature with DS18B20 (OneWire)
/* https://github.com/NordicPlayground/nrf51-TIMER-examples/blob/master/timer_example_timer_mode/main.c */
/*----------------------------------------------------------------------------------*/
/*                 Functions call to be used here in this module                    */
/*----------------------------------------------------------------------------------*/
#include      "Functions.h"

/*----------------------------------------------------------------------------------*/
/*            Common variables shared by the functions of this module               */
/* [WARNING]: By default you should prefer to declare local variables implemented   */
/* in functions when there is enough memory space to avoid shared variables.        */
/*----------------------------------------------------------------------------------*/
boolean                     CheckDS18B20Probe1, CheckDS18B20Probe3, CheckDS18B20Probe4;
boolean                     CheckI2cProbe1, CheckI2cProbe3, CheckI2cProbe4;
// Humidity probe
char                        ArrayForNewSlope[14];
char                        ArrayForNewYintercept[14];
uint16_t                    CountingForDryProbe1, CountingForDryProbe3, CountingForDryProbe4;
uint16_t                    CountingForWetProbe1, CountingForWetProbe3, CountingForWetProbe4;
char                        MyAnswer[64];
HumidityRH_ADC_t            HumMeas1, HumMeas3, HumMeas4;
float                       Slope;
float                       y_intercept_corrected;
// Temperature probe
uint8_t                     Device_Address[8];
uint8_t                     Probe1_Address[8];
uint8_t                     Probe3_Address[8];
uint8_t                     Probe4_Address[8];
uint8_t                     RomData[12];
SensorFeatures_t            FeaturesProbe1, FeaturesProbe3, FeaturesProbe4;
TemperaturesCF_t            TempProbe1, TempProbe3, TempProbe4;
// frames
char                        TabAsciiFunction[20];

/*----------------------------------------------------------------------------------*/
/*    External variables encountered in other modules and used in this module       */
/* [WARNING]: You must call in this module, the library or other module which refer */
/* to these variables declared as external here.                                    */
/*----------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------*/
/*                                Class instances                                   */
/* [WARNING]: Objects declared here have to target a constructor of a library       */
/* called by this module. The constructor which is a public methods with private    */
/* attributes. The declaration here is consistent, knowing that the call of         */
/* functions of a probe or electronic component imply two files more associated     */
/* to the project in which instances are representative  objects of the component   */
/* used.                                                                            */
/*----------------------------------------------------------------------------------*/
OneWire OneWire_Probe1(DS18S20Pin_GPIO0);
OneWire OneWire_Probe3(DS18S20Pin_GPIO1);
OneWire OneWire_Probe4(DS18S20Pin_GPIO2);
ADS1115 MyADC_Probe1(0x48);
ADS1115 MyADC_Probe3(0x4A);
ADS1115 MyADC_Probe4(0x4B);

/*----------------------------------------------------------------------------------*/
/*                 INTERRUPTION FUNCTIONS (Interruption vectors)                    */
/*----------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------*/
/*                          FUNCTIONS of this module                                */
/*----------------------------------------------------------------------------------*/
/****************************************************************************************************/
/* Polling function to check the presence of I2C devices.                                           */
/****************************************************************************************************/
void scanI2Cbus() {
  uint8_t NbrDevice = 0;
  uint8_t k;
  Serial.println(F("---------------------------"));
  Serial.println(F("Starting  I2C scan..."));
  for (k = 1; k < 128; k++) {
    Wire.beginTransmission(k);                        // If true, endTransmission() sends a stop message after transmission, releasing the I2C bus
    if (Wire.endTransmission() == 0) {                // if device is present
      NbrDevice++;                                    // number of device increased
      Serial.print(F("I2C used channel 0x"));
      if (k < 16) Serial.print("0");
      Serial.println(k, HEX);                         // hexadecimal address        
    }
  }
  Serial.println(F("SCAN COMPLETE"));
  Serial.print(F("I2C devices encountered: "));
  Serial.println(NbrDevice, DEC);
  Serial.println(F("---------------------------"));
}
/****************************************************************************************************/
/* identification of probes on I2C bus from which instance are declared.                            */
/* With 5 Volts power supply 5 V: ads1115.setGain(GAIN_ONE);  // 1x gain +/- 4.096V 1 bit = 2mV     */
/* With 3.3 Volts power supply: ads1115.setGain(GAIN_TWO);                                          */
/****************************************************************************************************/
void ADCStartUp(uint8_t ADC_Gain) {
  MyADC_Probe1.begin();                   // (ADDR tied to GND)
  MyADC_Probe3.begin();                   // (ADDR tied to SDA)
  MyADC_Probe4.begin();                   // (ADDR tied to SCL)
  MyADC_Probe1.setGain(ADC_Gain);
  MyADC_Probe3.setGain(ADC_Gain);
  MyADC_Probe4.setGain(ADC_Gain);
  MyADC_Probe1.setDataRate(ADS1X15_MODE_SINGLE);
  MyADC_Probe3.setDataRate(ADS1X15_MODE_SINGLE);
  MyADC_Probe4.setDataRate(ADS1X15_MODE_SINGLE);
  AcquireHumidity(Probe1);                // display for each probe one measure
  AcquireHumidity(Probe3);
  AcquireHumidity(Probe4);
  DividerTextForFunctions(80, '*');
}
/****************************************************************************************************/
/* Function to read the humidity measure from probes which are present.                             */
/* indice will change from 0 to 3 with Probe2 prohibited.                                           */
/****************************************************************************************************/
void AcquireHumidity(Probe_t ProbeSelected) {
  int16_t adc;
  float ddpADC;
  DividerTextForFunctions(80, '*');
  switch (ProbeSelected) {
    case Probe1:                    // construct an ads1115 at address 0x48 (ADDR tied to GND)
      HumMeas1.DigitalFromADC = MyADC_Probe1.readADC(Channel_ADC);
      if (HumMeas1.DigitalFromADC != -1) {
        CheckI2cProbe1 = true;
        Serial.println(F("\u2731 Probe1 values"));
        Serial.print(F("\t\tCounting value provided by ADC: "));
        Serial.println(HumMeas1.DigitalFromADC, DEC);
        HumMeas1.Potential = ((float)HumMeas1.DigitalFromADC / (float)ADS1115FullScale) * gain2_048;
        Serial.print(F("\t\tPotential: "));
        Serial.print(HumMeas1.Potential, 3);
        if (HumMeas1.Potential > 2) Serial.println(" Volts");
        else Serial.println(" Volt");
        HumMeas1.Humidity = (DefaultSlope * (float)HumMeas1.DigitalFromADC) + (float)Default_y_intercept;    // Value of humidity in Realtive Humidity unit (%)
        //HumMeas1.Humidity = (Slope * (float)HumMeas1.DigitalFromADC) + (float)y_intercept_corrected;
        Serial.print(F("\t\tHumidity: "));
        Serial.print(HumMeas1.Humidity, 2);
        Serial.println(" %RH");
      } else {
        CheckI2cProbe1 = false;
        Serial.println(F("Probe1 is not connected..."));
        return;
      }
      break;
    case Probe3:                    // construct an ads1115 at address 0x4A (ADDR tied to SDA)
      HumMeas3.DigitalFromADC = MyADC_Probe3.readADC(Channel_ADC);
      if (HumMeas3.DigitalFromADC != -1) {
        CheckI2cProbe3 = true;
        Serial.println(F("\u2731 Probe3 values"));
        Serial.print(F("\t\tCounting value provided by ADC: "));
        Serial.println(HumMeas3.DigitalFromADC, DEC);
        HumMeas3.Potential = ((float)HumMeas3.DigitalFromADC / (float)ADS1115FullScale) * gain2_048;
        Serial.print(F("\t\tPotential: "));
        Serial.print(HumMeas3.Potential, 3);
        if (HumMeas3.Potential > 2) Serial.println(" Volts");
        else Serial.println(" Volt");
        HumMeas3.Humidity = (DefaultSlope * (float)HumMeas3.DigitalFromADC) + (float)Default_y_intercept;    // Value of humidity in Realtive Humidity unit (%)
        //HumMeas3.Humidity = (Slope * (float)HumMeas3.DigitalFromADC) + (float)y_intercept_corrected;
        Serial.print(F("\t\tHumidity: "));
        Serial.print(HumMeas3.Humidity, 2);
        Serial.println(" %RH");
      } else {
        CheckI2cProbe3 = false;
        Serial.println(F("Probe3 is not connected..."));
        return;
      }
      break;
    case Probe4:                    // construct an ads1115 at address 0x4B (ADDR tied to SCL)
      HumMeas4.DigitalFromADC = MyADC_Probe4.readADC(Channel_ADC);
      if (HumMeas4.DigitalFromADC != -1) {
        CheckI2cProbe4 = true;
        Serial.println(F("\u2731 Probe4 values"));
        Serial.print(F("\t\tCounting value provided by ADC: "));
        Serial.println(HumMeas4.DigitalFromADC, DEC);
        HumMeas4.Potential = ((float)HumMeas4.DigitalFromADC / (float)ADS1115FullScale) * gain2_048;
        Serial.print(F("\t\tPotential: "));
        Serial.print(HumMeas4.Potential, 3);
        if (HumMeas4.Potential > 2) Serial.println(" Volts");
        else Serial.println(" Volt");
        HumMeas4.Humidity = (DefaultSlope * (float)HumMeas4.DigitalFromADC) + (float)Default_y_intercept;    // Value of humidity in Realtive Humidity unit (%)
        //HumMeas4.Humidity = (Slope * (float)HumMeas4.DigitalFromADC) + (float)y_intercept_corrected;
        Serial.print(F("\t\tHumidity: "));
        Serial.print(HumMeas4.Humidity, 2);
        Serial.println(" %RH");
      } else {
        CheckI2cProbe4 = false;
        Serial.println(F("Probe4 is not connected..."));
        return;
      }
      break;
  }
}
/****************************************************************************************************/
/* Text divider                                                                                     */
/****************************************************************************************************/
void DividerTextForFunctions(uint8_t nbr_carac, char caract) {
  uint8_t i;
  for (i = 0; i < nbr_carac; i++) Serial.print(caract);
  Serial.println();
}
/****************************************************************************************************/
/* Search all addresses from all OneWire sensors.                                                   */
/****************************************************************************************************/
void SearchAddressesOneWire() {
  boolean ProbeFound;
  uint8_t k;
  uint8_t *lclptr;
  ProbeFound = OneWire_Probe1.search(&Probe1_Address[0], true);
  if (ProbeFound == true) {
    Serial.println(F("\u2714 Temperature sensor on GPIO0"));
    if (OneWire_Probe1.crc8(&Probe1_Address[0], 7) != Probe1_Address[7]) Serial.println(F("\t\tCRC is not valid!"));
    else Serial.println(F("\t\tCRC is valid"));
    FeaturesProbe1.SensorType = I2CDeviceIdentifier(Probe1_Address[0]);
    lclptr = &Probe1_Address[0];
    Serial.print(F("\t\tSelected probe address at GPIO0: "));
    for (k = 0; k < 8; k++) {
      if (*lclptr > 15) Serial.print(*(lclptr++), HEX);
      else {
        Serial.print('0');
        Serial.print(*(lclptr++), HEX);
      }
      Serial.print(' ');
    }
    Serial.println();
    CheckDS18B20Probe1 = true;
  } else {
    Serial.println(F("\t\tProbe1 on GPIO0 does not exist."));
    CheckDS18B20Probe1 = false;
  }
  ProbeFound = OneWire_Probe3.search(&Probe3_Address[0], true);
  if (ProbeFound == true) {
    Serial.println(F("\u2714 Temperature sensor on GPIO1"));
    if (OneWire_Probe3.crc8(&Probe3_Address[0], 7) != Probe3_Address[7]) Serial.println(F("\t\tCRC is not valid!"));
    else Serial.println(F("\t\tCRC is valid"));
    FeaturesProbe3.SensorType = I2CDeviceIdentifier(Probe3_Address[0]);
    lclptr = &Probe3_Address[0];
    Serial.print(F("\t\tSelected probe address at GPIO1: "));
    for (k = 0; k < 8; k++) {
      if (*lclptr > 15) Serial.print(*(lclptr++), HEX);
      else {
        Serial.print('0');
        Serial.print(*(lclptr++), HEX);
      }
      Serial.print(' ');
    }
    Serial.println();
    CheckDS18B20Probe3 = true;
  } else {
    Serial.println(F("\t\tProbe3 on GPIO1 does not exist."));
    CheckDS18B20Probe3 = false;
  }
  ProbeFound = OneWire_Probe4.search(&Probe4_Address[0], true);
  if (ProbeFound == true) {
    Serial.println(F("\u2714 Temperature sensor on GPIO2"));
    if (OneWire_Probe4.crc8(&Probe4_Address[0], 7) != Probe4_Address[7]) Serial.println(F("\t\tCRC is not valid!"));
    else Serial.println(F("\t\tCRC is valid"));
    FeaturesProbe4.SensorType = I2CDeviceIdentifier(Probe4_Address[0]);
    lclptr = &Probe4_Address[0];
    Serial.print(F("\t\tSelected probe address at GPIO2: "));
    for (k = 0; k < 8; k++) {
      if (*lclptr > 15) Serial.print(*(lclptr++), HEX);
      else {
        Serial.print('0');
        Serial.print(*(lclptr++), HEX);
      }
      Serial.print(' ');
    }
    Serial.println();
    CheckDS18B20Probe4 = true;
  } else {
    Serial.println(F("\t\tProbe4 on GPIO2 does not exist."));
    CheckDS18B20Probe4 = false;
  }
}
/****************************************************************************************************/
/* Function to identify the type of the temperature sensor.                                         */
/* Mandatory function called with addresses search.                                                 */
/****************************************************************************************************/
uint8_t I2CDeviceIdentifier(uint8_t code) {
  uint8_t Type;
  switch (code) {
    case DS18S20MODEL:
      Serial.println(F("\t\tDevice = DS18S20"));          // or old DS1820
      Type = 1;
      break;
    case DS18B20MODEL:
      Serial.println(F("\t\tDevice = DS18B20"));
      Type = 0;
      break;
    case DS1822MODEL:
      Serial.println(F("\t\tDevice = DS1822"));
      Type = 0;
      break;
    default:
      Serial.println(F("\t\tDevice does not belong to Maxim family."));
      Type = 2;
      break;
  }
  return Type; 
}
/****************************************************************************************************/
/* Function to set resolution of each sensor which have been identified with one address.           */
/****************************************************************************************************/
void SetResolutionForAllDS18B20Sensors(Resolution_t ConvRes) {
  uint8_t Accuracy;
  if (CheckDS18B20Probe1 == true) {
    ReadScratchpadFromAddress(Probe1);
    Accuracy = RomData[4];
    if (Accuracy != ConvRes) {
      OneWire_Probe1.reset();
      OneWire_Probe1.select(Probe1_Address);              // fix address
      OneWire_Probe1.write(WRITESCRATCHPAD, 0);
      OneWire_Probe1.write(0x0);
      OneWire_Probe1.write(0x0);
      if (FeaturesProbe1.SensorType != 1) OneWire_Probe1.write(ConvRes);
    }
  }
  if (CheckDS18B20Probe3 == true) {
    ReadScratchpadFromAddress(Probe3);
    Accuracy = RomData[4];
    if (Accuracy != ConvRes) {
      OneWire_Probe3.reset();
      OneWire_Probe3.select(Probe3_Address);              // fix address
      OneWire_Probe3.write(WRITESCRATCHPAD, 0);
      OneWire_Probe3.write(0x0);
      OneWire_Probe3.write(0x0);
      if (FeaturesProbe3.SensorType != 1) OneWire_Probe3.write(ConvRes);
    }
  }
  if (CheckDS18B20Probe4 == true) {
    ReadScratchpadFromAddress(Probe4);
    Accuracy = RomData[4];
    if (Accuracy != ConvRes) {
      OneWire_Probe4.reset();
      OneWire_Probe4.select(Probe4_Address);              // fix address
      OneWire_Probe4.write(WRITESCRATCHPAD, 0);
      OneWire_Probe4.write(0x0);
      OneWire_Probe4.write(0x0);
      if (FeaturesProbe4.SensorType != 1) OneWire_Probe4.write(ConvRes);
    }
  }
}
/****************************************************************************************************/
/* Function to read temperatures on DS18B20 sensors which have been identified as available.        */
/****************************************************************************************************/
void DisplayTemperaturesValuesFromOneWire() {
  uint8_t present;
  uint8_t k;
  int16_t raw;
  int16_t mask;
  int16_t TwoComplement;
  uint8_t Config;
  if (CheckDS18B20Probe1 == true) {
    OneWire_Probe1.reset();                             // reset
    OneWire_Probe1.select(Probe1_Address);              // fix address
    OneWire_Probe1.write(STARTConvert, 0);              // conversion
    delay(750);
    present = ReadScratchpadFromAddress(Probe1);
    DisplayContentOfScratchpad(present, Probe1);
    raw = (RomData[1] << 8) | RomData[0];
    mask = raw & 0xF800;
    if (mask != 0) {
      TwoComplement = ~raw;
      TwoComplement += 1;
      TempProbe1.MeasTempC = (float)(TwoComplement * 0.0625);          // or divided by 16
      TempProbe1.MeasTempC *= -1.00f;
      TempProbe1.MeasTempF = (TempProbe1.MeasTempC * 1.8) + 32.0f;
    } else {
      TempProbe1.MeasTempC = (float)(raw * 0.0625);
      TempProbe1.MeasTempF = (TempProbe1.MeasTempC * 1.8) + 32.0f;
    }
    Serial.print(F("\u2714 Temperature measure for Probe1: "));
    Serial.print(TempProbe1.MeasTempC, 3);
    Serial.print(F(" °C    or    "));
    Serial.print(TempProbe1.MeasTempF, 3);
    Serial.println(F(" °F"));
  }
  if (CheckDS18B20Probe3 == true) {
    OneWire_Probe3.reset();                             // reset
    OneWire_Probe3.select(Probe3_Address);              // fix address
    OneWire_Probe3.write(STARTConvert, 0);              // conversion
    delay(750);
    present = ReadScratchpadFromAddress(Probe3);
    DisplayContentOfScratchpad(present, Probe3);
    raw = (RomData[1] << 8) | RomData[0];
    mask = raw & 0xF800;
    if (mask != 0) {
      TwoComplement = ~raw;
      TwoComplement += 1;
      TempProbe3.MeasTempC = (float)(TwoComplement * 0.0625);          // or divided by 16
      TempProbe3.MeasTempC *= -1.00f;
      TempProbe3.MeasTempF = (TempProbe3.MeasTempC * 1.8) + 32.0f;
    } else {
      TempProbe3.MeasTempC = (float)(raw * 0.0625);
      TempProbe3.MeasTempF = (TempProbe3.MeasTempC * 1.8) + 32.0f;
    }
    Serial.print(F("\u2714 Temperature measure for Probe3: "));
    Serial.print(TempProbe3.MeasTempC, 3);
    Serial.print(F(" °C    or    "));
    Serial.print(TempProbe3.MeasTempF, 3);
    Serial.println(F(" °F"));    
  }
  if (CheckDS18B20Probe4 == true) {
    OneWire_Probe4.reset();                             // reset
    OneWire_Probe4.select(Probe4_Address);              // fix address
    OneWire_Probe4.write(STARTConvert, 0);              // conversion
    delay(750);
    present = ReadScratchpadFromAddress(Probe4);
    DisplayContentOfScratchpad(present, Probe4);
    raw = (RomData[1] << 8) | RomData[0];
    mask = raw & 0xF800;
    if (mask != 0) {
      TwoComplement = ~raw;
      TwoComplement += 1;
      TempProbe4.MeasTempC = (float)(TwoComplement * 0.0625);          // or divided by 16
      TempProbe4.MeasTempC *= -1.00f;
      TempProbe4.MeasTempF = (TempProbe4.MeasTempC * 1.8) + 32.0f;
    } else {
      TempProbe4.MeasTempC = (float)(raw * 0.0625);
      TempProbe4.MeasTempF = (TempProbe4.MeasTempC * 1.8) + 32.0f;
    }
    Serial.print(F("\u2714 Temperature measure for Probe4: "));
    Serial.print(TempProbe4.MeasTempC, 3);
    Serial.print(F(" °C    or    "));
    Serial.print(TempProbe4.MeasTempF, 3);
    Serial.println(F(" °F"));    
  }
}
/****************************************************************************************************/
/* Function to read scratchpad of each DS18B20 using address knowned.                               */
/****************************************************************************************************/
uint8_t ReadScratchpadFromAddress(Probe_t TheSensor) {
  uint8_t k;
  uint8_t presenceAsserted;
  switch (TheSensor) {
    case Probe1:
      if (CheckDS18B20Probe1 == true) {
        presenceAsserted = OneWire_Probe1.reset();      // reset
        OneWire_Probe1.select(Probe1_Address);          // fix address
        OneWire_Probe1.write(READSCRATCHPAD, 0);        // read values
        memset(RomData, Null, sizeof(RomData));
        for (k = 0; k < 9; k++) RomData[k] = OneWire_Probe1.read();
      }
      break;
    case Probe2:
      break;
    case Probe3:
      if (CheckDS18B20Probe3 == true) {
        presenceAsserted = OneWire_Probe3.reset();      // reset
        OneWire_Probe3.select(Probe3_Address);          // fix address
        OneWire_Probe3.write(READSCRATCHPAD, 0);        // read values
        memset(RomData, Null, sizeof(RomData));
        for (k = 0; k < 9; k++) RomData[k] = OneWire_Probe3.read();
      }
      break;
    case Probe4:
      if (CheckDS18B20Probe4 == true) {
        presenceAsserted = OneWire_Probe4.reset();      // reset
        OneWire_Probe4.select(Probe4_Address);          // fix address
        OneWire_Probe4.write(READSCRATCHPAD, 0);        // read values
        memset(RomData, Null, sizeof(RomData));
        for (k = 0; k < 9; k++) RomData[k] = OneWire_Probe4.read();
      }
      break;
    default:
      break;
  }
  return presenceAsserted;
}
/****************************************************************************************************/
/* Function to display on terminal content of scratchpad.                                           */
/****************************************************************************************************/
void DisplayContentOfScratchpad(uint8_t Available, Probe_t TheSensor) {
  uint8_t k;
  DividerTextForFunctions(80, '-');
  Serial.println(F("\uFFED Data in registers (Scratchpad)"));
  Serial.print(F("\t\tSensor presence: "));
  if (Available == 1) Serial.println(F("yes"));
  else Serial.println(F("no"));
  Serial.print(F("\t\tContent of scratchpad: "));
  for (k = 0; k < 9; k++) {
    Serial.print("0x");
    if (RomData[k] < 10) Serial.print('0');
    Serial.print(RomData[k], HEX);
    Serial.print(' ');
  }
  Serial.print(F("\n\t\t CRC content: "));
  switch (TheSensor) {
    case Probe1:
      Serial.println(OneWire_Probe1.crc8(RomData, 8), HEX);
      break;
    case Probe3:
      Serial.println(OneWire_Probe3.crc8(RomData, 8), HEX);
      break;    
    case Probe4:
      Serial.println(OneWire_Probe4.crc8(RomData, 8), HEX);
      break;
  }
}
/****************************************************************************************************/
/* Prepares the payload of the frame. This mandatory function is called when joining status is      */
/* asserted. appData size is LORAWAN_APP_DATA_MAX_SIZE which is defined in "commissioning.h".       */
/* appData[] array is defined in LoRaWan_APP.h and appDataSize is a variable of LoRaWan_APP.h also. */
/* appDataSize max value is LORAWAN_APP_DATA_MAX_SIZE.                                              */
/* if enabled AT, don't modify LORAWAN_APP_DATA_MAX_SIZE, it may cause system hanging or failure.   */
/* if disabled AT, LORAWAN_APP_DATA_MAX_SIZE can be modified, the max value is reference to lorawan */
/* region and SF. For example, if use REGION_EU868, the max value for different DR can be found in  */
/* MaxPayloadOfDatarateEU868                                                                        */
/* static const uint8_t MaxPayloadOfDatarateEU868[] = {51, 51, 51, 115, 242, 242, 242, 242};        */
/* static const uint8_t DataratesEU868[]  = {12, 11, 10, 9, 8, 7, 7, 50};                           */
/* Vext or GPIO6 = LOW => involves for MOSFET AO7801 a conducting channel between source and drain. */
/* So using schematic, the control of this MOSFET allows the supply of the RGB Led.                 */
/* frame: HumShrink1(uint16_t), '%', tempProbe1(int16_t), 'C', HumShrink3(uint16_t), '%',           */
/*        tempProbe3(int16_t), 'C', HumShrink4(uint16_t), '%', tempProbe4(int16_t), 'C' => 21 bytes */
/* Frame example: 05462501294303C625012A43043A25012B43000056                                        */
/****************************************************************************************************/
void prepareTxFrame() {
  uint8_t NbrChar;
  uint8_t k;
  uint8_t *lclptr;
  pinMode(Vext, OUTPUT);            // GPIO6 which is not mentioned on schematic, here Vext should have been mentioned as Vext_CTL
  digitalWrite(Vext, LOW);          // control MOSFET canal P to allow VDD to be applied at output Vext to supply the RGB LED
  
  memset(appData, '\0', sizeof(appData));     // LoRaWan_APP.cpp (uint8_t appData[LORAWAN_APP_DATA_MAX_SIZE];)
  appDataSize = 0;                            // will be increased (attribute from LoRaWan_APP.cpp)
  AcquireTemperatureValuesFromAllProbes();    // all temperatures

  if (CheckDS18B20Probe1 == true) {
    HumMeas1.DigitalFromADC = MyADC_Probe1.readADC(Channel_ADC);                                        // send back an int16_t
    HumMeas1.Potential = ((float)HumMeas1.DigitalFromADC / (float)ADS1115FullScale) * gain2_048;
    #ifdef Calibration
      HumMeas1.Humidity = (DefaultSlope * (float)HumMeas1.DigitalFromADC) + (float)Default_y_intercept; // y = a.x + b with a < 0 et b = 185 %RH
    #else
      HumMeas1.Humidity = (Slope1 * (float)HumMeas1.DigitalFromADC) + (float)y_intercept_corrected1;    // y = a.x + b with a < 0 et b < 185 %RH
    #endif
    memset(TabAsciiFunction, Null, sizeof(TabAsciiFunction));
    NbrChar = ConvertFloatToStringWithSign(HumMeas1.Humidity, 2, &TabAsciiFunction[0]);
    for (k = 0; k < NbrChar; k++) appData[appDataSize++] = (uint8_t)TabAsciiFunction[k];
    appData[appDataSize++] = (uint8_t)('%');
    memset(TabAsciiFunction, Null, sizeof(TabAsciiFunction));
    NbrChar = ConvertFloatToStringWithSign(TempProbe1.MeasTempC, 2, &TabAsciiFunction[0]);
    for (k = 0; k < NbrChar; k++) appData[appDataSize++] = TabAsciiFunction[k];
    appData[appDataSize++] = (uint8_t)('C');
  }
  
  if (CheckDS18B20Probe3 == true) {
    HumMeas3.DigitalFromADC = MyADC_Probe3.readADC(Channel_ADC);                                        // send back an int16_t
    HumMeas3.Potential = ((float)HumMeas3.DigitalFromADC / (float)ADS1115FullScale) * gain2_048;
    #ifdef Calibration
      HumMeas3.Humidity = (DefaultSlope * (float)HumMeas3.DigitalFromADC) + (float)Default_y_intercept; // y = a.x + b with a < 0 et b = 185 %RH
    #else
      HumMeas3.Humidity = (Slope3 * (float)HumMeas3.DigitalFromADC) + (float)y_intercept_corrected3;    // y = a.x + b with a < 0 et b < 185 %RH
    #endif
    memset(TabAsciiFunction, Null, sizeof(TabAsciiFunction));
    NbrChar = ConvertFloatToStringWithSign(HumMeas3.Humidity, 2, &TabAsciiFunction[0]);
    for (k = 0; k < NbrChar; k++) appData[appDataSize++] = (uint8_t)TabAsciiFunction[k];
    appData[appDataSize++] = (uint8_t)('%');
    memset(TabAsciiFunction, Null, sizeof(TabAsciiFunction));
    NbrChar = ConvertFloatToStringWithSign(TempProbe3.MeasTempC, 2, &TabAsciiFunction[0]);
    for (k = 0; k < NbrChar; k++) appData[appDataSize++] = TabAsciiFunction[k];
    appData[appDataSize++] = (uint8_t)('C');
  }
  
  if (CheckDS18B20Probe4 == true) {
    HumMeas4.DigitalFromADC = MyADC_Probe4.readADC(Channel_ADC);                                        // send back an int16_t
    HumMeas4.Potential = ((float)HumMeas4.DigitalFromADC / (float)ADS1115FullScale) * gain2_048;
    #ifdef Calibration
      HumMeas4.Humidity = (DefaultSlope * (float)HumMeas4.DigitalFromADC) + (float)Default_y_intercept; // y = a.x + b with a < 0 et b = 185 %RH
    #else
      HumMeas4.Humidity = (Slope4 * (float)HumMeas4.DigitalFromADC) + (float)y_intercept_corrected4;    // y = a.x + b with a < 0 et b < 185 %RH
    #endif
    memset(TabAsciiFunction, Null, sizeof(TabAsciiFunction));
    NbrChar = ConvertFloatToStringWithSign(HumMeas4.Humidity, 2, &TabAsciiFunction[0]);
    for (k = 0; k < NbrChar; k++) appData[appDataSize++] = (uint8_t)TabAsciiFunction[k];
    appData[appDataSize++] = (uint8_t)('%');
    memset(TabAsciiFunction, Null, sizeof(TabAsciiFunction));
    NbrChar = ConvertFloatToStringWithSign(TempProbe4.MeasTempC, 2, &TabAsciiFunction[0]);
    for (k = 0; k < NbrChar; k++) appData[appDataSize++] = TabAsciiFunction[k];
    appData[appDataSize++] = (uint8_t)('C');
  }
  
  #ifdef DataChecking
    DividerTextForFunctions(100, '-');
    Serial.println(F("\u2714 Probe1"));
    Serial.print(F("\t\tInteger returned (uint6_t) from ADC: "));
    Serial.println(HumMeas1.DigitalFromADC, DEC);
    Serial.print(F("\t\tCorresponding voltage: "));
    Serial.print(HumMeas1.Potential, 3); Serial.println(" V");
    Serial.print(F("\t\tRelative humidity from probe1: "));
    Serial.print(HumMeas1.Humidity, 2); Serial.println(" %RH");
    Serial.print(F("\t\tTemperature from DS18B20 sensor as Probe1: "));
    Serial.print(TempProbe1.MeasTempC, 2); Serial.println(" °C");
    DividerTextForFunctions(20, '*');
    Serial.println(F("\u2714 Probe3"));
    Serial.print(F("\t\tInteger returned (uint6_t) from ADC: "));
    Serial.println(HumMeas3.DigitalFromADC, DEC);
    Serial.print(F("\t\tCorresponding voltage: "));
    Serial.print(HumMeas3.Potential, 3); Serial.println(" V");
    Serial.print(F("\t\tRelative humidity from probe3: "));
    Serial.print(HumMeas3.Humidity, 2); Serial.println(" %RH");
    Serial.print(F("\t\tTemperature from DS18B20 sensor as Probe3: "));
    Serial.print(TempProbe3.MeasTempC, 2); Serial.println(" °C");
    DividerTextForFunctions(20, '*');
    Serial.println(F("\u2714 Probe4"));
    Serial.print(F("\t\tInteger returned (uint6_t) from ADC: "));
    Serial.println(HumMeas4.DigitalFromADC, DEC);
    Serial.print(F("\t\tCorresponding voltage: "));
    Serial.print(HumMeas4.Potential, 3); Serial.println(" V");
    Serial.print(F("\t\tRelative humidity from probe4: "));
    Serial.print(HumMeas4.Humidity, 2); Serial.println(" %RH");
    Serial.print(F("\t\tTemperature from DS18B20 sensor as Probe4: "));
    Serial.print(TempProbe4.MeasTempC, 2); Serial.println(" °C");
    DividerTextForFunctions(20, '*');
    Serial.print(F("\uFFED Frame which will be sended: "));
    lclptr = &appData[0];
    do {
      Serial.print(*(lclptr++), HEX);
    } while (*lclptr != '\0');
    Serial.println();
  #endif
}
/****************************************************************************************************/
/* Function to acquire the temperatures from all DS18B20 sensors connected.                         */
/****************************************************************************************************/
void AcquireTemperatureValuesFromAllProbes() {
  uint8_t k;
  int16_t raw;
  int16_t mask;
  int16_t TwoComplement;
  uint8_t Config;
  if (CheckDS18B20Probe1 == true) {
    OneWire_Probe1.reset();                             // reset
    OneWire_Probe1.select(Probe1_Address);              // fix address
    OneWire_Probe1.write(STARTConvert, 0);              // conversion
    delay(750);
    ReadScratchpadFromAddress(Probe1);
    raw = (RomData[1] << 8) | RomData[0];
    mask = raw & 0xF800;
    if (mask != 0) {
      TwoComplement = ~raw;
      TwoComplement += 1;
      TempProbe1.MeasTempC = (float)(TwoComplement * 0.0625);          // or divided by 16
      TempProbe1.MeasTempC *= -1.00f;
      TempProbe1.MeasTempF = (TempProbe1.MeasTempC * 1.8) + 32.0f;
    } else {
      TempProbe1.MeasTempC = (float)(raw * 0.0625);
      TempProbe1.MeasTempF = (TempProbe1.MeasTempC * 1.8) + 32.0f;
    }
  }
  if (CheckDS18B20Probe3 == true) {
    OneWire_Probe3.reset();                             // reset
    OneWire_Probe3.select(Probe3_Address);              // fix address
    OneWire_Probe3.write(STARTConvert, 0);              // conversion
    delay(750);
    ReadScratchpadFromAddress(Probe3);
    raw = (RomData[1] << 8) | RomData[0];
    mask = raw & 0xF800;
    if (mask != 0) {
      TwoComplement = ~raw;
      TwoComplement += 1;
      TempProbe3.MeasTempC = (float)(TwoComplement * 0.0625);          // or divided by 16
      TempProbe3.MeasTempC *= -1.00f;
      TempProbe3.MeasTempF = (TempProbe3.MeasTempC * 1.8) + 32.0f;
    } else {
      TempProbe3.MeasTempC = (float)(raw * 0.0625);
      TempProbe3.MeasTempF = (TempProbe3.MeasTempC * 1.8) + 32.0f;
    }
  }
  if (CheckDS18B20Probe4 == true) {
    OneWire_Probe4.reset();                             // reset
    OneWire_Probe4.select(Probe4_Address);              // fix address
    OneWire_Probe4.write(STARTConvert, 0);              // conversion
    delay(750);
    ReadScratchpadFromAddress(Probe4);
    raw = (RomData[1] << 8) | RomData[0];
    mask = raw & 0xF800;
    if (mask != 0) {
      TwoComplement = ~raw;
      TwoComplement += 1;
      TempProbe4.MeasTempC = (float)(TwoComplement * 0.0625);          // or divided by 16
      TempProbe4.MeasTempC *= -1.00f;
      TempProbe4.MeasTempF = (TempProbe4.MeasTempC * 1.8) + 32.0f;
    } else {
      TempProbe4.MeasTempC = (float)(raw * 0.0625);
      TempProbe4.MeasTempF = (TempProbe4.MeasTempC * 1.8) + 32.0f;
    }  
  }
}
/****************************************************************************************************/
/* Function to replace the equivalent method dtostrf.                                               */
/* char *dtostrf(double val, signed char width, unsigned char prec, char *s)                        */
/* The aim is to fill the char array identified with the values of a double or a float number.      */
/* Serial object methods like print and println do not allow display the correct content of a real. */
/* The signed char width : number of alphanumeric values, comma included ('.').                     */
/* Unsigned char prec : precision of the float or number of digits just after the comma.            */
/* essential link to get an example for conversion: https://www.esp8266.com/viewtopic.php?t=3592    */
/****************************************************************************************************/
uint8_t ConvertFloatToStringWithSign(float ConvertFloat, uint8_t NbrDecimals, char *DestArray) {
  uint8_t k;
  boolean NegativeSign;
  uint8_t NbrAsciiCharOfString = 0;         // string width : number of ASCII digit returned, comma included.
  uint32_t IntegerResult;
  char ScratchArray[15];                    // to convert positive uint32_t in ASCII format
  uint8_t NbrChar;
  uint8_t CommaPosition;

  if (ConvertFloat < 0.0f) {
    NegativeSign = true;
    *(DestArray++) = '-';
    ConvertFloat *= -1.0f;
    NbrAsciiCharOfString++;
  } else NegativeSign = false;
  memset(ScratchArray, Null, sizeof(ScratchArray));

  switch (NbrDecimals) {
    case 0:
      IntegerResult = (uint32_t)ConvertFloat;
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
      *DestArray = Null;
      NbrAsciiCharOfString += NbrChar;
      break;
    case 1:
      IntegerResult = (uint32_t)(ConvertFloat * 10.0f);
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);  // NbrChar = 3
      if (IntegerResult < 10) {
        *(DestArray++) = (char)0x30;
        *(DestArray++) = '.';
        for (k = 0; k < (1 - NbrChar); k++) *(DestArray++) = (char)0x30;
        for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
        NbrAsciiCharOfString += (1 + 2);
      } else {
        CommaPosition = NbrChar - 1;                                                                          // CommaPosition = 2
        for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
        *(DestArray++) = '.';
        *(DestArray++) = ScratchArray[CommaPosition];
        *DestArray = Null;
        NbrAsciiCharOfString += (NbrChar + 1);
      }
      break;
    case 2:
      IntegerResult = (uint32_t)(ConvertFloat * 100.0f);
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      if (IntegerResult < 100) {
        *(DestArray++) = (char)0x30;
        *(DestArray++) = '.';
        for (k = 0; k < (2 - NbrChar); k++) *(DestArray++) = (char)0x30;
        for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
        NbrAsciiCharOfString += (2 + 2);
      } else {
        CommaPosition = NbrChar - 2;
        for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
        *(DestArray++) = '.';
        for (k = 0; k < 2; k++) *(DestArray++) = ScratchArray[CommaPosition + k];
        *DestArray = Null;
        NbrAsciiCharOfString += (NbrChar + 1);
      }
      break;
    case 3:
      IntegerResult = (uint32_t)(ConvertFloat * 1000.0f);
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      if (IntegerResult < 1000) {
        *(DestArray++) = (char)0x30;
        *(DestArray++) = '.';
        for (k = 0; k < (3 - NbrChar); k++) *(DestArray++) = (char)0x30;
        for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
        NbrAsciiCharOfString += (3 + 2);
      } else {
        CommaPosition = NbrChar - 3;
        for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
        *(DestArray++) = '.';
        for (k = 0; k < 3; k++) *(DestArray++) = ScratchArray[CommaPosition + k];
        *DestArray = Null;
        NbrAsciiCharOfString += (NbrChar + 1);
      }
      break;
    case 4:
      IntegerResult = (uint32_t)(ConvertFloat * 10000.0f);
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      if (IntegerResult < 10000) {
        *(DestArray++) = (char)0x30;
        *(DestArray++) = '.';
        for (k = 0; k < (4 - NbrChar); k++) *(DestArray++) = (char)0x30;
        for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
        NbrAsciiCharOfString += (4 + 2);
      } else {
        CommaPosition = NbrChar - 4;
        for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
        *(DestArray++) = '.';
        for (k = 0; k < 4; k++) *(DestArray++) = ScratchArray[CommaPosition + k];
        *DestArray = Null;
        NbrAsciiCharOfString += (NbrChar + 1);
      }
      break;
    case 5:
      IntegerResult = (uint32_t)(ConvertFloat * 100000.0f);
      //Serial.print(F("IntegerResult: ")); Serial.println(IntegerResult, DEC);             // OK
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      // Serial.print(F("NbrChar: ")); Serial.println(NbrChar, DEC);                        // OK
      if (IntegerResult < 100000) {
        *(DestArray++) = (char)0x30;
        *(DestArray++) = '.';
        for (k = 0; k < (5 - NbrChar); k++) *(DestArray++) = (char)0x30;
        for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
        NbrAsciiCharOfString += (5 + 2);
      } else {
        CommaPosition = NbrChar - 5;
        for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
        *(DestArray++) = '.';
        for (k = 0; k < 5; k++) *(DestArray++) = ScratchArray[CommaPosition + k];
        *DestArray = Null;
        NbrAsciiCharOfString += (NbrChar + 1);
      }
      break;
    case 6:
      IntegerResult = (uint32_t)(ConvertFloat * 1000000.0f);
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      if (IntegerResult < 1000000) {
        *(DestArray++) = (char)0x30;
        *(DestArray++) = '.';
        for (k = 0; k < (6 - NbrChar); k++) *(DestArray++) = (char)0x30;
        for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
        NbrAsciiCharOfString += (6 + 2);
      } else {
        CommaPosition = NbrChar - 6;
        for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
        *(DestArray++) = '.';
        for (k = 0; k < 6; k++) *(DestArray++) = ScratchArray[CommaPosition + k];
        *DestArray = Null;
        NbrAsciiCharOfString += (NbrChar + 1);
      }
      break;
    case 7:
      IntegerResult = (uint32_t)(ConvertFloat * 10000000.0f);
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      if (IntegerResult < 10000000) {                     // float lower than 1
        *(DestArray++) = (char)0x30;
        *(DestArray++) = '.';
        for (k = 0; k < (7 - NbrChar); k++) *(DestArray++) = (char)0x30;
        for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
        NbrAsciiCharOfString += (7 + 2);
      } else {
        CommaPosition = NbrChar - 7;
        for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
        *(DestArray++) = '.';
        for (k = 0; k < 7; k++) *(DestArray++) = ScratchArray[CommaPosition + k];
        *DestArray = Null;
        NbrAsciiCharOfString += (NbrChar + 1);
      }
      break;
    case 8:
      IntegerResult = (uint32_t)(ConvertFloat * 100000000.0f);
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      if (IntegerResult < 100000000) {
        *(DestArray++) = (char)0x30;
        *(DestArray++) = '.';
        for (k = 0; k < (8 - NbrChar); k++) *(DestArray++) = (char)0x30;
        for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
        NbrAsciiCharOfString += (8 + 2);
      } else {
        CommaPosition = NbrChar - 8;
        for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
        *(DestArray++) = '.';
        for (k = 0; k < 8; k++) *(DestArray++) = ScratchArray[CommaPosition + k];
        *DestArray = Null;
        NbrAsciiCharOfString += (NbrChar + 1);
      }
      break;
    case 9:
      IntegerResult = (uint32_t)(ConvertFloat * 1000000000.0f);
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      if (IntegerResult < 1000000000) {
        *(DestArray++) = (char)0x30;
        *(DestArray++) = '.';
        for (k = 0; k < (9 - NbrChar); k++) *(DestArray++) = (char)0x30;
        for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
        NbrAsciiCharOfString += (9 + 2);
      } else {
        CommaPosition = NbrChar - 9;
        for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
        *(DestArray++) = '.';
        for (k = 0; k < 9; k++) *(DestArray++) = ScratchArray[CommaPosition + k];
        *DestArray = Null;
        NbrAsciiCharOfString += (NbrChar + 1);
      }
      break;
    default:        // only TWO decimals
      IntegerResult = (uint32_t)(ConvertFloat * 100.0f);
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      CommaPosition = NbrChar - 2;
      for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
      *(DestArray++) = '.';
      for (k = 0; k < 2; k++) *(DestArray++) = ScratchArray[CommaPosition + k];
      *DestArray = Null;
      NbrAsciiCharOfString += (NbrChar + 1);
      break;
  }
  return NbrAsciiCharOfString;
}
/****************************************************************************************************/
/* Function to convert an uint32_t (hexadecimal form) into a decimal ASCII representation to        */
/* replace sprintf for long integer. The conversion is made from low significant bit to high        */
/* significant bit and the number of significant digit is returned.                                 */
/* The ASCII string returned in the char array is left aligned.                                     */
/****************************************************************************************************/
uint8_t ConvertUint32ToASCIICharArray(char *ptrTAB, uint8_t DimArray, uint32_t valToConvert) {   // 10 possible characters from 0 to 4,294,967,295
  char *ptrINIT;
  uint8_t m, k;
  uint8_t indice;
  uint8_t result_modulo;
  uint32_t result_division;
  ptrINIT = ptrTAB;
  for (m = 0; m < DimArray; m++) *(ptrTAB++) = Null;                    // initialisation of the array
  indice = 9;                                                           // the low significant digit in the char array (unity)
  ptrTAB = ptrINIT;
  ptrTAB += indice * sizeof(char);                                      // to fix the low digit
  do {
    result_modulo = (uint8_t)(valToConvert % 0x0A);
    *(ptrTAB--) = (char)(result_modulo + 0x30);                         // ASCII char to display
    indice--;
    result_division = (valToConvert - result_modulo) / 0x0A;
    valToConvert = result_division;                                     // new value for which we have to define the modulo
  } while (result_division > 9);                                        // if result is minor than 9, the loop is closed
  *ptrTAB = (char)(0x30 + result_division);
  ptrTAB = ptrINIT;                                                     // first position in the array
  ptrINIT += indice * sizeof(char);                                     // to retrieve the last position of the most significant digit
  for (k = 0; k < DimArray - indice; k++) *(ptrTAB++) = *(ptrINIT++);   // to retrieve an array starting with the fisrt position [0]
  for (k = DimArray - indice; k < DimArray; k++) *(ptrTAB++) = 0x20;    // DimArray (10) is the dimension of the array Scratch_tab[]
  return DimArray - indice;                                             //
}
/****************************************************************************************************/
/* Function to convert an uint32_t (hexadecimal form) into a decimal ASCII representation to        */
/* replace sprintf for long integer. sprintf do not function with long integer.                     */
/* The conversion is made from low significant digit to high significant digit and the number of    */
/* significant digits is returned. The conversion begin with the discover of the unit digit and she */
/* ends by the most significant digit of the decimal representation.                                */
/* principle : modulo by 0x0A (first operation) => result have to be removed from initial value.    */
/* modulo =>          integer value strictly inferior at 0x0A, either the result between 0 and 9.   */
/* soustraction =>    we reduce the value by the result obtained by the modulo function.            */
/*                    (significative digit).                                                        */
/* division =>        division by 0x0A, to fix the new result from which we will applied the modulo */
/*                    function.                                                                     */
/* EXAMPLE: value 0x0358 to convert                                                                 */
/*                    0x358 % A = 6 => 0x358 - 6 = 0x352 => 0x352 / A = 0x55                        */
/*                    0x55 % A = 5 => 0x55 - 5 = 0x50 => 0x50 / A = 0x8                             */
/*                    Result => 0x358 => '8' '5' '6'                                                */
/* The search is ended when the reuslt of the division by A give only one digit strictly inferior   */
/* than 0x0A.                                                                                       */
/* The array which receive ASCII characters with a decimal representation has a dimension of 20     */
/* characters. This array have to be public to be shared by all equivalent functions of this module.*/
/* ConvUintxx_tToAsciiChar[10];     // 2^32 = 4 294 967 296                                         */
/* ConvUintxx_tToAsciiChar[20];     // 2^64 = 18 446 744 073 709 551 616                            */
/* The ASCII string returned in the char array is left aligned.                                     */
/* The array dimension must be at least over 10 cells to represent uint32_t integers.               */
/****************************************************************************************************/
uint8_t ConvertUint32ToDecimalCharAsciiArray(char *ptrTAB, uint8_t ArrayLength, uint32_t valToConvert) {    // 10 possible characters from 0 to 4,294,967,295 (2^32)
  char *ptrINIT;
  uint8_t k;
  uint8_t index;
  uint8_t result_modulo;
  uint32_t result_division;
  ptrINIT = ptrTAB;
  for (k = 0; k < ArrayLength; k++) *(ptrTAB++) = Null;                     // to initialize the array ConvUintxx_tToAsciiChar[20]
  index = ArrayLength - 1;                                                  // the low significant digit in the char array (unity) from 0 to dimension - 1 for index
  ptrTAB = ptrINIT;
  ptrTAB += index * sizeof(char);                                           // to fix the low digit in the char array
  do {
    result_modulo = (uint8_t)(valToConvert % 0x0A);
    *(ptrTAB--) = (char)(result_modulo + 0x30);                             // ASCII char to display
    index--;
    result_division = (valToConvert - result_modulo) / 0x0A;
    valToConvert = result_division;                                         // new value for which we have to define the modulo
  } while (result_division > 9);                                            // if result is between 0 and 9, the loop stops and we can identify all characters to be displayed
  *ptrTAB = (char)(0x30 + result_division);
  ptrTAB = ptrINIT;                                                         // first position in the array
  ptrINIT += index * sizeof(char);                                          // to retrieve the last position of the most significant digit
  for (k = 0; k < ArrayLength - index; k++) *(ptrTAB++) = *(ptrINIT++);     // to retrieve an array starting with the fisrt position [0]
  *ptrTAB = Null;
  return ArrayLength - index;
}
/****************************************************************************************************/
/* Function to get an answer from operator using terminal. This function waits a mandatory answer   */
/* from the terminal. The short answer is 'Y' (Yes) or 'N' (No) but this function also would be     */
/* useful to retrieve other long response.                                                          */
/* common char array is MyAnswer[20].                                                               */
/****************************************************************************************************/
char *FillMyAnswerArray() {
  uint8_t k = 0;
  int InComingByte = Null;
  memset(MyAnswer, Null, sizeof(MyAnswer));       // char MyAnswer[64];
  do {                                            // polling loop
    if (Serial.available() != 0) {
      InComingByte = Serial.read();               // virtual int read(void); from HardwareSerial.h due to larger datas from UART buffer
      MyAnswer[k++] = (char)InComingByte;
      if (InComingByte == '\n' || InComingByte == '\r') break;  // We wait the answer of the operator Yes or No
    }
  } while (1);
  MyAnswer[k - 1] = Null;                         // to replace '\n' or '\r'
  Serial.print(F("[System] Your answer is: "));
  Serial.println(MyAnswer);                       // global array shared by all functions of this module
  return &MyAnswer[0];
}
/****************************************************************************************************/
/* Function to calibrate humidity probe using the terminal to converse between electronic and       */
/* operator.                                                                                        */
/****************************************************************************************************/
void CalibrateHumidityProbe() {
  boolean Probe1Calibration = false;
  boolean Probe3Calibration = false;
  boolean Probe4Calibration = false;
  char *LclPtr;
  uint8_t k;
  uint8_t NbrCharOfString;

  DividerTextForFunctions(80, '_');
  Serial.println(F("[Advice] For the probe calibrations, you need to store value as compilation directives."));
  Serial.println(F("[Query] Do you need to calibrate humidity probes? Y/N (y/n):"));
  LclPtr = FillMyAnswerArray();
  if (*LclPtr == 'Y' || *LclPtr == 'y') {
    Serial.println(F("[Advice] \uFFED You have to calibrate all three probes 1, 3 and 4"));
    do {
      Serial.println(F("[Query] What number probe do you want to calibrate? (1, 3 or 4):"));
      LclPtr = FillMyAnswerArray();
      if (*LclPtr == '1') {
        Serial.println(F("[Advice] First you need to dig up the probe1 and dry it."));
        Serial.println(F("[Query] Is the probe1 in the air and is it dry? Y/N (y/n):"));
        LclPtr = FillMyAnswerArray();
        if (*LclPtr == 'N' || *LclPtr == 'n') {
          Serial.println(F("[Advice] I am waiting your availability for as long as it takes..."));
          Serial.println(F("[Query] Is the probe1 in the air and is it dry? Y/N (y/n):"));
          do {
            LclPtr = FillMyAnswerArray();
          } while (*LclPtr != 'Y' || *LclPtr != 'y');
        }
        HumMeas1.DigitalFromADC = MyADC_Probe1.readADC(Channel_ADC);
        CountingForDryProbe1 = HumMeas1.DigitalFromADC;
        Serial.println(F("[Measure] Values for dry probe1"));
        DisplayADCcounting_potential(CountingForDryProbe1);
        Serial.println(F("\n[Advice] Second you need to dive probe1 entirely into a water glass."));
        Serial.println(F("\n[Advice] It is better to wait several minutes before answering yes."));
        Serial.println(F("[Query] Is the probe1 immersed in water? Y/N (y/n):"));
        LclPtr = FillMyAnswerArray();
        if (*LclPtr == 'N' || *LclPtr == 'n') {
          Serial.println(F("[Advice] I am waiting your availability for as long as it takes..."));
          Serial.println(F("[Query] Is the probe1 immersed in water? Y/N (y/n):"));
          do {
            LclPtr = FillMyAnswerArray();
          } while (*LclPtr != 'Y' || *LclPtr != 'y');
        }
        HumMeas1.DigitalFromADC = MyADC_Probe1.readADC(Channel_ADC);
        CountingForWetProbe1 = HumMeas1.DigitalFromADC;
        Serial.println(F("[Measure] Values for wet probe1"));
        DisplayADCcounting_potential(CountingForWetProbe1);
        Slope = 90.0f / (float)(CountingForWetProbe1 - CountingForDryProbe1);             // a' (float)
        Serial.print(F("\n\uFFED New slope for Probe1 has been calculated: "));
        memset(ArrayForNewSlope, Null, sizeof(ArrayForNewSlope));
        NbrCharOfString = ConvertFloatToStringWithSign(Slope, 7, &ArrayForNewSlope[0]);
        LclPtr = &ArrayForNewSlope[0];
        for (k = 0; k < NbrCharOfString; k++) Serial.print(*(LclPtr++));
        Serial.println();
        y_intercept_corrected = (float)(90.0f - (float)(CountingForWetProbe1 * Slope));   // second correction with the new slope
        Serial.print(F("\uFFED New y intercept for Probe1 has been calculated: "));
        memset(ArrayForNewYintercept, Null, sizeof(ArrayForNewYintercept));
        NbrCharOfString = ConvertFloatToStringWithSign(y_intercept_corrected, 4, &ArrayForNewYintercept[0]);
        LclPtr = &ArrayForNewYintercept[0];
        for (k = 0; k < NbrCharOfString; k++) Serial.print(*(LclPtr++));
        Serial.println();
        Probe1Calibration = true;
      }
      if (*LclPtr == '3') {
        Serial.println(F("[Advice] First you need to dig up the probe3 and dry it."));
        Serial.println(F("[Query] Is the probe3 in the air and is it dry? Y/N (y/n):"));
        LclPtr = FillMyAnswerArray();
        if (*LclPtr == 'N' || *LclPtr == 'n') {
          Serial.println(F("[Advice] I am waiting your availability for as long as it takes..."));
          Serial.println(F("[Query] Is the probe3 in the air and is it dry? Y/N (y/n):"));
          do {
            LclPtr = FillMyAnswerArray();
          } while (*LclPtr != 'Y' || *LclPtr != 'y');
        }
        HumMeas3.DigitalFromADC = MyADC_Probe3.readADC(Channel_ADC);
        CountingForDryProbe3 = HumMeas3.DigitalFromADC;
        Serial.println(F("[Measure] Values for dry probe3"));
        DisplayADCcounting_potential(CountingForDryProbe3);
        Serial.println(F("\n[Advice] Second you need to dive probe3 entirely into a water glass."));
        Serial.println(F("\n[Advice] It is better to wait several minutes before answering yes."));
        Serial.println(F("[Query] Is the probe3 immersed in water? Y/N (y/n):"));
        LclPtr = FillMyAnswerArray();
        if (*LclPtr == 'N' || *LclPtr == 'n') {
          Serial.println(F("[Advice] I am waiting your availability for as long as it takes..."));
          Serial.println(F("[Query] Is the probe3 immersed in water? Y/N (y/n):"));
          do {
            LclPtr = FillMyAnswerArray();
          } while (*LclPtr != 'Y' || *LclPtr != 'y');
        }
        HumMeas3.DigitalFromADC = MyADC_Probe3.readADC(Channel_ADC);
        CountingForWetProbe3 = HumMeas3.DigitalFromADC;
        Serial.println(F("[Measure] Values for wet probe3"));
        DisplayADCcounting_potential(CountingForWetProbe3);
        Slope = 90.0f / (float)(CountingForWetProbe3 - CountingForDryProbe3);             // a' (float)
        Serial.print(F("\n\uFFED New slope for Probe3 has been calculated: "));
        memset(ArrayForNewSlope, Null, sizeof(ArrayForNewSlope));
        NbrCharOfString = ConvertFloatToStringWithSign(Slope, 7, &ArrayForNewSlope[0]);
        LclPtr = &ArrayForNewSlope[0];
        for (k = 0; k < NbrCharOfString; k++) Serial.print(*(LclPtr++));
        Serial.println();
        y_intercept_corrected = (float)(90.0f - (float)(CountingForWetProbe3 * Slope));   // second correction with the new slope
        Serial.print(F("\uFFED New y intercept for Probe3 has been calculated: "));
        memset(ArrayForNewYintercept, Null, sizeof(ArrayForNewYintercept));
        NbrCharOfString = ConvertFloatToStringWithSign(y_intercept_corrected, 4, &ArrayForNewYintercept[0]);
        LclPtr = &ArrayForNewYintercept[0];
        for (k = 0; k < NbrCharOfString; k++) Serial.print(*(LclPtr++));
        Serial.println();
        Probe3Calibration = true;        
      }
      if (*LclPtr == '4') {
        Serial.println(F("[Advice] First you need to dig up the probe4 and dry it."));
        Serial.println(F("[Query] Is the probe4 in the air and is it dry? Y/N (y/n):"));
        LclPtr = FillMyAnswerArray();
        if (*LclPtr == 'N' || *LclPtr == 'n') {
          Serial.println(F("[Advice] I am waiting your availability for as long as it takes..."));
          Serial.println(F("[Query] Is the probe4 in the air and is it dry? Y/N (y/n):"));
          do {
            LclPtr = FillMyAnswerArray();
          } while (*LclPtr != 'Y' || *LclPtr != 'y');
        }
        HumMeas4.DigitalFromADC = MyADC_Probe4.readADC(Channel_ADC);
        CountingForDryProbe4 = HumMeas4.DigitalFromADC;
        Serial.println(F("[Measure] Values for dry probe4"));
        DisplayADCcounting_potential(CountingForDryProbe4);
        Serial.println(F("\n[Advice] Second you need to dive probe4 entirely into a water glass."));
        Serial.println(F("\n[Advice] It is better to wait several minutes before answering yes."));
        Serial.println(F("[Query] Is the probe4 immersed in water? Y/N (y/n):"));
        LclPtr = FillMyAnswerArray();
        if (*LclPtr == 'N' || *LclPtr == 'n') {
          Serial.println(F("[Advice] I am waiting your availability for as long as it takes..."));
          Serial.println(F("[Query] Is the probe4 immersed in water? Y/N (y/n):"));
          do {
            LclPtr = FillMyAnswerArray();
          } while (*LclPtr != 'Y' || *LclPtr != 'y');
        }
        HumMeas4.DigitalFromADC = MyADC_Probe4.readADC(Channel_ADC);
        CountingForWetProbe4 = HumMeas4.DigitalFromADC;
        Serial.println(F("[Measure] Values for wet probe4"));
        DisplayADCcounting_potential(CountingForWetProbe4);
        Slope = 90.0f / (float)(CountingForWetProbe4 - CountingForDryProbe4);             // a' (float)
        Serial.print(F("\n\uFFED New slope for Probe4 has been calculated: "));
        memset(ArrayForNewSlope, Null, sizeof(ArrayForNewSlope));
        NbrCharOfString = ConvertFloatToStringWithSign(Slope, 7, &ArrayForNewSlope[0]);
        LclPtr = &ArrayForNewSlope[0];
        for (k = 0; k < NbrCharOfString; k++) Serial.print(*(LclPtr++));
        Serial.println();
        y_intercept_corrected = (float)(90.0f - (float)(CountingForWetProbe4 * Slope));   // second correction with the new slope
        Serial.print(F("\uFFED New y intercept for Probe4 has been calculated: "));
        memset(ArrayForNewYintercept, Null, sizeof(ArrayForNewYintercept));
        NbrCharOfString = ConvertFloatToStringWithSign(y_intercept_corrected, 4, &ArrayForNewYintercept[0]);
        LclPtr = &ArrayForNewYintercept[0];
        for (k = 0; k < NbrCharOfString; k++) Serial.print(*(LclPtr++));
        Serial.println();
        Probe4Calibration = true;        
      }
    } while (Probe1Calibration == false || Probe3Calibration == false || Probe4Calibration == false);
  }
  DividerTextForFunctions(80, '_');
}
/****************************************************************************************************/
/* Function to display only potentiel measured and digital output from ADC.                         */
/****************************************************************************************************/
void DisplayADCcounting_potential(uint16_t counting) {
  float Potential;
  Serial.print(F("\u2714 Counting from ADC: "));
  Serial.println(counting, DEC);
  Potential = ((float)counting * gain2_048) / (float)ADS1115FullScale;
  Serial.print(F("\u2714 Voltage measured from ADS1115: "));
  Serial.print(Potential);
  if (Potential >= 2.0) Serial.println(F(" volts"));
  else Serial.println(F(" volt"));
}






/* ######################################################################################################## */
// END of file
