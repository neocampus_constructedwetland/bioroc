// NOTE:  The Single Board Computer awaited here is: CubeCell-Board (HTCC-AB01).
//        Special thanks to Rob Tillaart for his ADS1115 library https://github.com/RobTillaart
//
/*----------------------------------------------------------------------------------*/
/*                 Functions call and used only in the main module                  */
/*----------------------------------------------------------------------------------*/
#include "Functions.h"
#include "private-credentials.h" // define: devEui, appEui, appKey

/*************************************************************************************
 * set LoraWan_RGB to Active,the RGB active in loraWan
 * RGB red means sending;
 * RGB purple means joined done;
 * RGB blue means RxWindow1;
 * RGB yellow means RxWindow2;
 * RGB green means received done;
*************************************************************************************/

/*----------------------------------------------------------------------------------*/
/*                                 OTAA mode                                        */
/*----------------------------------------------------------------------------------*/
// This Eui must be in little-endian format, so least-significant-byte first. 
// When copying an EUI from ttnctl output, this means to reverse the bytes.             
extern uint8_t devEui[8] PROGMEM; // = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};             // your DevEUI provided by your server LoRaWAN application only for OTAA mode (LoRaWAN neOCampus)
extern uint8_t appEui[8] PROGMEM; // = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};             // your AppEUI provided by your server LoRaWAN application only for OTAA mode (LoRaWAN neOCampus)
// This key should be in big endian format (or, since it is not really a number but a block of memory, endianness does not really apply).
extern uint8_t appKey[16] PROGMEM; // = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};  // your application key (LoRaWAN neOCampus)

/*----------------------------------------------------------------------------------*/
/*                                 ABP mode                                         */
/*----------------------------------------------------------------------------------*/
extern uint8_t nwkSKey[] PROGMEM; // = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};   // your network session key for ABP mode
extern uint8_t appSKey[] PROGMEM; // = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};   // your application session key for ABP mode
extern uint32_t devAddr; // = (uint32_t)0x12345678;                                                                                        // the identifier of your device for ABP mode

/*----------------------------------------------------------------------------------*/
/*                LoraWan channelsmask, default channels 0-7                        */
/*----------------------------------------------------------------------------------*/
uint16_t userChannelsMask[6] = {0x00FF,0x0000,0x0000,0x0000,0x0000,0x0000};                 // http://community.heltec.cn/t/cubecell-board-manager-issues-v0-0-6-and-v0-0-7/1584/2

/*LoraWan region, select in arduino IDE tools*/
LoRaMacRegion_t loraWanRegion = LORAMAC_REGION_EU868;           // LoRaMacRegion_t loraWanRegion = ACTIVE_REGION;
/*LoraWan Class, Class A and Class C are supported*/
DeviceClass_t  loraWanClass = CLASS_A;                          // DeviceClass_t  loraWanClass = LORAWAN_CLASS;
/*the application data transmission duty cycle.  value in [ms].*/
uint32_t appTxDutyCycle = 240000;                               // => 4 minutes (defined also in LoRaWan_APP.h)
/*OTAA or ABP: bool overTheAirActivation = LORAWAN_NETMODE;*/
bool overTheAirActivation = false;                              // bool overTheAirActivation = LORAWAN_NETMODE;
/*ADR enable*/
bool loraWanAdr = LORAWAN_ADR;
/* set LORAWAN_Net_Reserve ON, the node could save the network info to flash, when node reset not need to join again */
bool keepNet = LORAWAN_NET_RESERVE;
/* Indicates if the node is sending confirmed or unconfirmed messages */
bool isTxConfirmed = LORAWAN_UPLINKMODE;
/* Application port */
uint8_t appPort = 173;
/* Number of trials to transmit the frame, if the LoRaMAC layer did not receive an acknowledgment.
 * The MAC performs a datarate adaptation, according to the LoRaWAN Specification V1.0.2, chapter 18.4, according to the following table:
 * Transmission nb | Data Rate
 * ----------------|-----------
 * 1 (first)       | DR
 * 2               | DR
 * 3               | max(DR-1,0)
 * 4               | max(DR-1,0)
 * 5               | max(DR-2,0)
 * 6               | max(DR-2,0)
 * 7               | max(DR-3,0)
 * 8               | max(DR-3,0)
 * Note, that if NbTrials is set to 1 or 2, the MAC will not decrease
 * the datarate, in case the LoRaMAC layer did not receive an acknowledgment */
uint8_t confirmedNbTrials = 4;



void setup() {
  boardInitMcu();                   // function defined in header file board.h and definition is in asr_board.c
  Serial.begin(115200);
  #if(AT_SUPPORT)
    enableAt();
  #endif
  Wire.begin();                                         // I2C
  scanI2Cbus();                                         // I2C
  ADCStartUp(ADS1X15_PGA_4_096V);                       // I2C
  SearchAddressesOneWire();                             // OneWire
  SetResolutionForAllDS18B20Sensors(ResConv12bits);     // OneWire
  DisplayTemperaturesValuesFromOneWire();
  #ifdef Calibration
    CalibrateHumidityProbe();
  #endif
  deviceState = DEVICE_STATE_INIT;          // enum eDeviceState_LoraWan deviceState; LoRaWan_APP.cpp
  LoRaWAN.ifskipjoin();                     // LoRaWAN_APP.cpp (LoRawanClass)
}

void loop() {
  switch (deviceState) {
    case DEVICE_STATE_INIT:
    {
      #if(LORAWAN_DEVEUI_AUTO)
        LoRaWAN.generateDeveuiByChipID();
      #endif
      #if(AT_SUPPORT)
        getDevParam();
      #endif
      printDevParam();
      LoRaWAN.init(loraWanClass, loraWanRegion);  // void LoRaWanClass::init(DeviceClass_t lorawanClass,LoRaMacRegion_t region)
      deviceState = DEVICE_STATE_JOIN;
      break;
    }
    case DEVICE_STATE_JOIN:
    {
      LoRaWAN.join();
      break;
    }
    case DEVICE_STATE_SEND:
    {
      prepareTxFrame();
      LoRaWAN.send();
      deviceState = DEVICE_STATE_CYCLE;
      break;
    }
    case DEVICE_STATE_CYCLE:
    {
      // Schedule next packet transmission
      txDutyCycleTime = appTxDutyCycle + randr(0, APP_TX_DUTYCYCLE_RND);
      LoRaWAN.cycle(txDutyCycleTime);
      deviceState = DEVICE_STATE_SLEEP;
      break;
    }
    case DEVICE_STATE_SLEEP:
    {
      LoRaWAN.sleep();
      break;
    }
    default:
    {
      deviceState = DEVICE_STATE_INIT;
      break;
    }
  }
}


/* ######################################################################################################## */
// END of file
