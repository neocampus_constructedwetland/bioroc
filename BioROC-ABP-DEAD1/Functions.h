/* ******************************************************************************************** */
/* Global functions for the project.                                                            */
/* ******************************************************************************************** */
// Author: Jean-Louis Druilhe (jean-louis.druilhe@univ-tlse3.fr)

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_        1
/*----------------------------------------------------------------------------------*/
/*         Here the libraries call with all classes and their methods               */
/* [WARNING]: All instantiate objects in this module allow the usage of all methods */
/* and attributes declared in these libraries on the condition that they are called */
/* here.                                                                            */
/*----------------------------------------------------------------------------------*/
#include      "LoRaWan_APP.h"
#include      <ADS1X15.h>
#include      <Arduino.h>
#include      <Wire.h>                  // I2C library
#include      <OneWire.h>               // global library from Adafruit
//#include      <C:\Users\druilhe\AppData\Local\Arduino15\packages\CubeCell\hardware\CubeCell\1.3.0\libraries\OneWire\OneWire.h>
//#include      <DallasTemperature.h>
//#include      <Adafruit_ADS1X15.h>
#include      <string.h>
#include      <stdio.h>
#include      <stdint.h>
#include      <stdlib.h>                /* atof() function or strtol() */

/*----------------------------------------------------------------------------------*/
/*                         C++ Preprocessor directives                              */
/* [NOTICE]: Shared preprocessor directives which are used by the main file or in   */
/* each module where they are declared. As modules are called by the main file, the */
/* directive is active in the main file and in the module himself.                  */
/* Each module needs to activate or inhibit directive in the header file obviously. */
/*----------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*                 Local directives to check some states when it is necessary                     */
/*------------------------------------------------------------------------------------------------*/
#define       messagesON
//#define       ADS115Connected
#define       DataChecking
//#define       Calibration

/*------------------------------------------------------------------------------------------------*/
/*                                       SYSTEM CONSTANTS                                         */
/*------------------------------------------------------------------------------------------------*/
//#define         LF                      0x0A          // '\n' this acronym is already used by the core
//#define         CR                      0x0D          // '\r' this acronym is already used by the core
#define         Space                   0x20
#define         Null                    '\0'          // 0 or '\0'
#define         F_CPU                   64000000
#define         PCLK16M                 16000000

/*------------------------------------------------------------------------------------------------*/
/*                                  Directives and constants                                      */
/*------------------------------------------------------------------------------------------------*/
// Ressources specific to this card CubeCell HTCC-AB01
#define         DS18S20Pin_GPIO0            GPIO0         // first probe DS18B20 (Probe1) connected on GPIO0
#define         DS18S20Pin_GPIO1            GPIO1         // second probe DS18B20 (Probe3) connected on GPIO1
#define         DS18S20Pin_GPIO2            GPIO2         // third probe DS18B20 (Probe4) connected on GPIO2
// Timers
// OneWire commands
#define         STARTConvert            0x44          // Tells device to take a temperature reading and put it on the scratchpad
#define         COPYSCRATCH             0x48          // Copy EEPROM
#define         READSCRATCHPAD          0xBE          // Read EEPROM
#define         WRITESCRATCHPAD         0x4E          // Write to EEPROM
#define         RECALLSCRATCH           0xB8          // Reload from last known
#define         READPOWERSUPPLY         0xB4          // Determine if device needs parasite power
#define         ALARMSEARCH             0xEC          // Query bus for devices with an alarm condition
#define         SEARCHROM               0xF0          // to identify the ROM codes of all devices on the same bus
#define         READROM                 0x33          // this command can only be used when one device is connected
#define         SKIPROM                 0xCC          // to address all devices without sending any ROM code
#define         DS18S20MODEL            0x10
#define         DS18B20MODEL            0x28
#define         DS1822MODEL             0x22
// Scratchpad locations
#define         TEMP_LSB                0
#define         TEMP_MSB                1
#define         HIGH_ALARM_TEMP         2
#define         LOW_ALARM_TEMP          3
#define         CONFIGURATION           4
#define         INTERNAL_BYTE           5
#define         COUNT_REMAIN            6
#define         COUNT_PER_C             7
#define         SCRATCHPAD_CRC          8
// Device resolution and accuracy
#define         TEMP_9_BIT              0x1F          //  9 bit
#define         TEMP_10_BIT             0x3F          // 10 bit
#define         TEMP_11_BIT             0x5F          // 11 bit
#define         TEMP_12_BIT             0x7F          // 12 bit
#define         Channel_ADC             1
#define         DefaultSlope            -0.0078       // slope relation
#define         Default_y_intercept     185
#define         Slope1                  -0.0106597
#define         y_intercept_corrected1  153.4253
#define         Slope3                  -0.0106926
#define         y_intercept_corrected3  153.2683
#define         Slope4                  -0.0106019
#define         y_intercept_corrected4  152.1380
#define         gain1_024               1.024
#define         gain2_048               2.048
#define         gain4_096               4.096
#define         ADS1115FullScale        32767
#define         Sign_Mask               0x80000000    // for float numbers defined with 4 bytes
#define         ADS1X15_MODE_CONTINUE   0x0000
#define         ADS1X15_MODE_SINGLE     0x0100
#define         ADS1X15_PGA_6_144V      0x0000        //  voltage
#define         ADS1X15_PGA_4_096V      0x0200
#define         ADS1X15_PGA_2_048V      0x0400        //  default
#define         ADS1X15_PGA_1_024V      0x0600
#define         ADS1X15_PGA_0_512V      0x0800
#define         ADS1X15_PGA_0_256V      0x0A00


// Humidity probe
#define         DefaultCountsDryProbe   18600
#define         DefaultCountsWetProbe   7260
// SSD Card
#define         UniversalTime           "UTC,"
#define         SinceJanuary1970        "UNIX,"


/*------------------------------------------------------------------------------------------------*/
/*               Predefined data types (Data Types and Predefined Structures)                     */
/* Definition types using keyword typedef and often in usage with typedef enum or typedef struct. */
/*------------------------------------------------------------------------------------------------*/
typedef enum Probes : uint8_t {
  Probe1 = 1,
  Probe2,
  Probe3,
  Probe4
} Probe_t;

typedef struct SensorFeatures {
  uint8_t SensorType;
  uint8_t Resolution;
} SensorFeatures_t;

typedef struct TemperaturesCF {
  float MeasTempC;
  float MeasTempF;
} TemperaturesCF_t;

typedef struct HumidityRH_ADC {
  int16_t DigitalFromADC;
  float Potential;
  float Humidity;
} HumidityRH_ADC_t;

typedef enum Resolution : uint8_t {
  ResConv9bits  = 0x1F,         //  9 bit
  ResConv10bits = 0x3F,         // 10 bit
  ResConv11bits = 0x5F,         // 11 bit
  ResConv12bits = 0x7F          // 12 bit
} Resolution_t;

/*------------------------------------------------------------------------------------------------*/
/*                          Function prototypes or functions interface                            */
/*------------------------------------------------------------------------------------------------*/
void scanI2Cbus(void);
void ADCStartUp(uint8_t);
void AcquireHumidity(Probe_t);
void DividerTextForFunctions(uint8_t, char);
void SearchAddressesOneWire(void);
uint8_t I2CDeviceIdentifier(uint8_t);
void SetResolutionForAllDS18B20Sensors(Resolution_t);
void DisplayTemperaturesValuesFromOneWire(void);
uint8_t ReadScratchpadFromAddress(Probe_t);
void DisplayContentOfScratchpad(uint8_t, Probe_t);
void prepareTxFrame(void);
void AcquireTemperatureValuesFromAllProbes(void);
uint8_t ConvertFloatToStringWithSign(float, uint8_t, char *);
uint8_t ConvertUint32ToASCIICharArray(char *, uint8_t, uint32_t); 
uint8_t ConvertUint32ToDecimalCharAsciiArray(char *, uint8_t, uint32_t);
char *FillMyAnswerArray(void);
void CalibrateHumidityProbe(void);
void DisplayADCcounting_potential(uint16_t);







#endif /* FONCTIONS_H_ */
/* ######################################################################################################## */
// END of file
